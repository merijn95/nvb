package sentence

// TODO Parse to an abstract syntax tree

import (
	"errors"
	"fmt"
	"log"
	"strings"
)

type Parser struct {
	scanner *Scanner
	store   Store

	current Token
}

var ErrTokenType = errors.New("token type mismatch")

func (p *Parser) Parse() error {
	for p.current.Type != EOF {
		switch p.current.Type {
		case Word:
			left := p.current.Lexeme
			err := p.Consume(Word)
			if err != nil {
				return err
			}

			sentences, err := p.store.Find(left)
			if err != nil {
				log.Println(err)
				continue
			}

			if len(sentences) == 0 {
				continue
			}

			if p.current.Type == EOF {
				fmt.Println(left)
				break
			}


			// We *know* that sentences has just one value.
			sentence := sentences[0]
			right := p.current.Lexeme
			err  = p.Consume(p.current.Type)
			if err != nil {
				return err
			}

			var found bool
			for _, child := range sentence.Children {
				if strings.EqualFold(child.Value, right) {
					found = true
					fmt.Printf("%s %s\n", left, right)
					break
				}
			}

			if !found {
				fmt.Println(left)
			}
		}
	}

	return errors.New("unable to parse")
}

func (p *Parser) Consume(tokenType TokenType) error {
	if p.current.Type != tokenType {
		return ErrTokenType
	}

	current, _ := p.scanner.Next()

	p.current = current
	return nil
}

func NewParser(store Store, scanner *Scanner) *Parser {
	p := &Parser{
		scanner: scanner,
		store:   store,
	}

	token, err := p.scanner.Next()
	if err != nil {
		log.Fatalln(err)
	}

	p.current = token

	return p
}
