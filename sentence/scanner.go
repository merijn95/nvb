package sentence

import (
	"errors"
	"strings"
)

type Scanner struct {
	sentence string
	position int
	current  string

	store Store
}

type TokenType int

type Token struct {
	Lexeme string
	Type   TokenType
}

func (t Token) ResolveType() string {
	switch t.Type {
	case Word:
		return "word"
	case Mixed:
		return "mixed"
	case EOF:
		return "eof"
	default:
		return ""
	}
}

var ErrEOF = errors.New("EOF")

const (
	Word = iota
	Mixed
	EOF
)

func (s *Scanner) Next() (Token, error) {
	for {
		if s.isWhitespace() {
			_ = s.Advance()
			continue
		}

		if s.isLetter() {
			sentence := s.Sentence()
			_ = s.Advance()
			return sentence, nil
		}

		return Token{Lexeme: "", Type: EOF}, ErrEOF
	}
}

func (s *Scanner) Advance() error {
	if s.position+1 >= len(s.sentence) {
		s.current = ""
		return nil
	}
	s.position++
	s.current = string(s.sentence[s.position])
	return nil
}

func (s *Scanner) Peek() string {
	if s.position+1 >= len(s.sentence) {
		return ""
	}

	return string(s.sentence[s.position+1])
}

func (s *Scanner) isLetter(cur ...string) bool {
	current := s.current
	if len(cur) == 1 {
		current = cur[0]
	}

	return (current >= "a" && current <= "z") || (current >= "A" && current <= "Z")
}

func (s *Scanner) isWhitespace(cur ...string) bool {
	current := s.current
	if len(cur) == 1 {
		current = cur[0]
	}

	return current == " "
}

func (s *Scanner) Sentence() Token {
	var sb strings.Builder
	sb.WriteString(s.current)

	next := s.Peek()

	deleteForbiddenCharacters := func() {
		for s.isWhitespace(next) || next == "-" {
			_ = s.Advance()
			next = s.Peek()
		}
	}

	deleteForbiddenCharacters()

	for s.isLetter(next) {
		sb.WriteString(strings.ToLower(next))
		_ = s.Advance()

		createToken := func(sentences []Sentence) Token {
			var tokType TokenType
			if len(sentences) == 1 {
				tokType = sentences[0].Type
			} else {
				// Deeper reflection is needed: a length bigger than one implies that
				// there are more ways of interpreting this part of the sentence.
				// Therefore, the token type is mixed.
				tokType = Mixed
			}

			return Token{
				Type:   tokType,
				Lexeme: sb.String(),
			}
		}

		sentences, _ := s.store.Find(sb.String())
		if len(sentences) > 0 {
			return createToken(sentences)
		}

		children, _ := s.store.FindChildren(sb.String())
		if len(children) > 0  {
			return createToken(children)
		}

		next = s.Peek()
		deleteForbiddenCharacters()
	}

	return Token{}
}

func NewScanner(store Store, sentence string) *Scanner {
	scanner := &Scanner{
		sentence: sentence,
		position: 0,
	}

	scanner.current = string(sentence[scanner.position])

	scanner.store = store
	return scanner
}
