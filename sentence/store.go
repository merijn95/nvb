package sentence

import (
	"strings"
)

type Sentence struct {
	Value    string
	Type     TokenType
	Children []Sentence
}

type Store interface {
	Find(s string) ([]Sentence, error)
	FindChildren(s string) ([]Sentence, error)
}

type FakeStore struct {
}


// Note: O(n^2) is slow for something like this; we should really use a good NoSQL database provider
// that can fix this 'issue'
func (f *FakeStore) FindChildren(s string) ([]Sentence, error) {
	res := data()

	var sentences []Sentence
	for _, sentence := range res {
		for _, child := range sentence.Children {
			if strings.EqualFold(child.Value, s) {
				sentences = append(sentences, child)
			}
		}
	}

	return sentences, nil
}

func (f *FakeStore) Find(s string) ([]Sentence, error) {
	// TODO Create a value that can be both an adjective and a noun.
	res := data()

	var sentences []Sentence
	for _, sentence := range res {
		if strings.EqualFold(sentence.Value, s) {
			sentences = append(sentences, sentence)
		}
	}

	return sentences, nil
}

func data() []Sentence {
	return []Sentence{
		{
			Value: "industrieschilder",
			Type:  Word,
		},
		{
			Value: "orderpicker",
			Type:  Word,
			Children: []Sentence{
				{
					Value: "hoi",
					Type: Word,
				},
			},
		},
		{
			Value: "planner",
			Type:  Word,
			Children: []Sentence{
				{
					Value: "servicemonteurs",
					Type:  Word,
				},
			},
		},
	}
}
