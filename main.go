package main

import (
	"nvbalgorithm/sentence"
)

func main() {
	store := &sentence.FakeStore{}
	inputs := []string{"pl-AnnEr SeRvICE-monteurs", "industrieschilder", "orderpicker", "orderpicker hoi"}
	for _, input := range inputs {
		scanner := sentence.NewScanner(store, input)
		parser := sentence.NewParser(store, scanner)

		parser.Parse()
	}
}
